CREATE TABLE `users`
(
    `id`         INT          NOT NULL AUTO_INCREMENT,
    `email`      VARCHAR(128) NOT NULL,
    `address`    TEXT         NOT NULL,
    `created_at` DATETIME     NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `posts`
(
    `id`       INT          NOT NULL AUTO_INCREMENT,
    `title`    VARCHAR(128) NOT NULL,
    `headline` VARCHAR(256) NOT NULL,
    `content`  TEXT         NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;
